#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_arrowLeft_clicked();

    void on_restart_clicked();

    void on_arrowRight_clicked();

    void on_Menu_clicked();

    void on_Search_clicked();

private:
    Ui::MainWindow *ui;
};







#endif // MAINWINDOW_H
