#ifndef MENU_H
#define MENU_H

#include <QWidget>

namespace Ui {
class Menu;
}

class Menu : public QWidget
{
    Q_OBJECT

public:
    explicit Menu(QWidget *parent = 0);
    ~Menu();

private slots:
    void on_story_clicked();

    void on_cookies_clicked();

    void on_favorites_clicked();

private:
    Ui::Menu *ui;
};

#endif // MENU_H
